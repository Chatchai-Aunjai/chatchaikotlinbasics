package com.example.chatchaikotlinbasics.unit2

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.cardview.widget.CardView
import com.example.chatchaikotlinbasics.R
import com.example.chatchaikotlinbasics.unit1.BirthdayActivity
import com.example.chatchaikotlinbasics.unit1.DiceRollerActivity
import com.example.chatchaikotlinbasics.unit1.LemonadeActivity

class Unit2Activity : AppCompatActivity() {
    private var cardView1: CardView? = null
    private var cardView2: CardView? = null
    private var cardView3: CardView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_unit2)

        cardView1 = findViewById<CardView>(R.id.tip_time_card)
        cardView2 = findViewById<CardView>(R.id.unit2_2)
        cardView3 = findViewById<CardView>(R.id.unit2_3)

        cardView1!!.setOnClickListener {
            val intent = Intent(this, TipTimeActivity::class.java)
            startActivity(intent)
        }
        cardView2!!.setOnClickListener {
            val intent = Intent(this, DiceRollerActivity::class.java)
            startActivity(intent)
        }
        cardView3!!.setOnClickListener {
            val intent = Intent(this, LemonadeActivity::class.java)
            startActivity(intent)
        }
    }
}