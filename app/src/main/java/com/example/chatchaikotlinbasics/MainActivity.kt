package com.example.chatchaikotlinbasics

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.cardview.widget.CardView
import com.example.chatchaikotlinbasics.unit1.LemonadeActivity
import com.example.chatchaikotlinbasics.unit1.Unit1Activity
import com.example.chatchaikotlinbasics.unit2.Unit2Activity

class MainActivity : AppCompatActivity() {

    private var cardView1: CardView? = null
    private var cardView2: CardView? = null
    private var cardView3: CardView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        cardView1 = findViewById<CardView>(R.id.unit1_card)
        cardView2 = findViewById<CardView>(R.id.unit2_card)
        cardView3 = findViewById<CardView>(R.id.unit3_card)

        cardView1!!.setOnClickListener {
            val intent = Intent(this, Unit1Activity::class.java)
            startActivity(intent)
        }
        cardView2!!.setOnClickListener {
            val intent = Intent(this, Unit2Activity::class.java)
            startActivity(intent)
        }
        cardView3!!.setOnClickListener {
            val intent = Intent(this, LemonadeActivity::class.java)
            startActivity(intent)
        }
    }
}