package com.example.chatchaikotlinbasics.unit1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.cardview.widget.CardView
import com.example.chatchaikotlinbasics.R

class Unit1Activity : AppCompatActivity() {

    private var cardView1: CardView? = null
    private var cardView2: CardView? = null
    private var cardView3: CardView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_unit1)

        cardView1 = findViewById<CardView>(R.id.birthdayCard)
        cardView2 = findViewById<CardView>(R.id.diceRoller)
        cardView3 = findViewById<CardView>(R.id.lemonade)

        cardView1!!.setOnClickListener {
            val intent = Intent(this, BirthdayActivity::class.java)
            startActivity(intent)
        }
        cardView2!!.setOnClickListener {
            val intent = Intent(this, DiceRollerActivity::class.java)
            startActivity(intent)
        }
        cardView3!!.setOnClickListener {
            val intent = Intent(this, LemonadeActivity::class.java)
            startActivity(intent)
        }
    }
}