package com.example.chatchaikotlinbasics.unit1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.TextView
import com.example.chatchaikotlinbasics.R

class DebugActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_debug)
        division()
    }

private val TAG = "MainActivity"
fun division() {
    val numerator = 60
    var denominator = 4
    repeat(4) {
        Thread.sleep(10000)
        Log.v(TAG, "${numerator / denominator}")
        findViewById<TextView>(R.id.division_textview).text = "${numerator / denominator}"
        denominator--
    }
}
}